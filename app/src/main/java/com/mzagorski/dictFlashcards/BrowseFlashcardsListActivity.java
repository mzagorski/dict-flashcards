package com.mzagorski.dictFlashcards;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.mzagorski.dictFlashcards.adapters.FlashcardListAdapter;
import com.mzagorski.dictFlashcards.dao.DictFlashcardsDbHelper;
import com.mzagorski.dictFlashcards.service.FlashcardList;


public class BrowseFlashcardsListActivity extends AppCompatActivity {

    public static final String FLASHCARD_SET_ID = "FLASHCARD_SET_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.browse_flashcard_list);
        ListView listView = (ListView) findViewById(R.id.flashcard_list);
        DictFlashcardsDbHelper helper = new DictFlashcardsDbHelper(this);
        SQLiteDatabase db = helper.getWritableDatabase();
        Long flashcardSetId = this.getIntent().getExtras().getLong(FLASHCARD_SET_ID);
        FlashcardList list = new FlashcardList(db, flashcardSetId);
        FlashcardListAdapter adapter = new FlashcardListAdapter(this, FlashcardListAdapter.toCursor(list.getFlashcards()), 0);
        listView.setAdapter(adapter);
    }
}
