package com.mzagorski.dictFlashcards;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.mzagorski.dictFlashcards.dao.DictFlashcardsDbHelper;
import com.mzagorski.dictFlashcards.dao.LanguageDao;
import com.mzagorski.dictFlashcards.dao.TranslationDao;
import com.mzagorski.dictFlashcards.model.Language;
import com.mzagorski.dictFlashcards.model.Setting;
import com.mzagorski.dictFlashcards.model.Translation;

import java.util.List;

public class EditTranslationActivity extends AppCompatActivity {

    public static final String TRANSLATION = "TRANSLATION";


    private LanguageDao languageDao;
    private TranslationDao translationDao;
    Spinner langsFromSpinner;
    Spinner langsToSpinner;
    Language langFrom;
    Language langTo;
    Button saveButton;
    Translation translation = new Translation();
    List<Language> langs;

    EditText wordEdit;
    EditText translEdit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_translation);
        if(this.getIntent().getExtras() != null && this.getIntent().getExtras().containsKey(TRANSLATION)) {
            this.translation = (Translation)this.getIntent().getExtras().get(TRANSLATION);
        }

        DictFlashcardsDbHelper helper  = new DictFlashcardsDbHelper(this);
        final SQLiteDatabase db = helper.getWritableDatabase();
        languageDao = new LanguageDao(db);
        translationDao = new TranslationDao(db);
        langs = languageDao.selectAll();

        if(translation.id > 0) {
            translation = translationDao.findById(translation.id);
        }

        if(translation.langFrom > 0) {
            langFrom = languageDao.findById(translation.langFrom);
        } else {
            langFrom = languageDao.findFromSettings(Setting.KEY_TRANS_LANG_FROM);
        }

        if(translation.langTo > 0) {
            langTo = languageDao.findById(translation.langTo);
        } else {
            langTo = languageDao.findFromSettings(Setting.KEY_TRANS_LANG_TO);
        }

        wordEdit = (EditText)findViewById(R.id.edit_transl_word);
        wordEdit.setText(translation.word);
        translEdit = (EditText)findViewById(R.id.edit_transl_transl);
        translEdit.setText(translation.translation);

        langsFromSpinner = (Spinner)findViewById(R.id.edit_transl_lang_from);
        langsToSpinner = (Spinner)findViewById(R.id.edit_transl_lang_to);

        saveButton = (Button)findViewById(R.id.edit_transl_save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                translation.langFrom = langs.get((int) langsFromSpinner.getSelectedItemId()).id;
                translation.langTo = langs.get((int) langsToSpinner.getSelectedItemId()).id;
                translation.word = wordEdit.getText().toString().trim();
                translation.translation = translEdit.getText().toString().trim();

                if (translationIsValid()) {
                    if (translation.id > 0) {
                        translationDao.update(translation);
                    } else {
                        translationDao.insert(translation);
                    }
                    finish();
                } else {
                    new AlertDialog.Builder(EditTranslationActivity.this)
                            .setTitle("Translation is invalid").setMessage("Please correct invalid fields before save.")
                            .setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            }).show();
                }

            }
        });

        refreshLangsFromSpinner();
        refreshLangsToSpinner();

        if(translation.id >0) {
            langsFromSpinner.setEnabled(false);
            langsToSpinner.setEnabled(false);
        }
    }

    private boolean translationIsValid() {
        Translation t = translation;
        return t.langFrom > 0 && t.langTo > 0 && t.langFrom != t.langTo
                && t.translation != null && !t.translation.trim().isEmpty() && t.word != null && !t.word.trim().isEmpty();
    }


    private void refreshLangsFromSpinner() {
        int selected = langs.indexOf(langFrom);
        ArrayAdapter<String> langsFromAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, langsToArray(langs));
        langsFromSpinner.setAdapter(langsFromAdapter);
        langsFromSpinner.setSelection(selected);
    }

    private void refreshLangsToSpinner() {
        int selected = langs.indexOf(langTo);
        ArrayAdapter<String> langsToAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, langsToArray(langs));
        langsToSpinner.setAdapter(langsToAdapter);
        langsToSpinner.setSelection(selected);
    }


    private String[] langsToArray(List<Language> langs) {
        String[] arr = new String[langs.size()];
        for(int i = 0; i< langs.size(); i++) {
            arr[i] = langs.get(i).englishName;
        }
        return arr;
    }
}
