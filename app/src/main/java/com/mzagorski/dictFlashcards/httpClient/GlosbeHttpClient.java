package com.mzagorski.dictFlashcards.httpClient;


import android.database.MatrixCursor;

import com.mzagorski.dictFlashcards.model.Language;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class GlosbeHttpClient {

    private final OkHttpClient client = new OkHttpClient();


    public List<OnlineTranslation> translate(final Language langFrom, final Language langTo, final String word) throws IOException, JSONException {
        String url = buildUrl(langFrom.iso639_3Code, langTo.iso639_3Code, word);
        Request request = new Request.Builder().url(url).build();
        Response response = client.newCall(request).execute();
        return parseResponseJson(response.body().string(), word);
    }


    private List<OnlineTranslation> parseResponseJson(String jsonContent, String word) throws JSONException, IOException {
        JSONObject json = new JSONObject(jsonContent);
        if(!"ok".equals(json.getString("result"))) {
            throw new IOException("Invalid call result: " + json.getString("result"));
        }
        JSONArray translations = json.getJSONArray("tuc");
        List<OnlineTranslation> trans = new LinkedList<>();
        for(int i = 0; i < translations.length(); i++) {
            JSONObject translationWithDesc = translations.getJSONObject(i);
            if (translationWithDesc.has("phrase")) {
                JSONObject translation = translations.getJSONObject(i).getJSONObject("phrase");
                OnlineTranslation oTrans = new OnlineTranslation();
                oTrans.word = word;
                oTrans.translation = translation.getString("text");
                if(oTrans.word != null && oTrans.translation != null) {
                    trans.add(oTrans);
                }
            }
        }
        return trans;
    }


    private String buildUrl(final String langFrom, final String langTo, final String word) {
        return String.format("https://glosbe.com/gapi/translate?from=%s&dest=%s&format=json&phrase=%s&pretty=false", langFrom, langTo, word);
    }


    public static MatrixCursor toMatrixCursor(List<OnlineTranslation> translations) {
        String[] columns = new String[] {  "word", "translation", "_id" };
        MatrixCursor matrixCursor = new MatrixCursor(columns);
        for(int i = 0; i< translations.size(); i++) {
            OnlineTranslation t = translations.get(i);
            matrixCursor.addRow(new Object[] { t.word, t.translation, new Integer(i) });
        }
        return matrixCursor;
    }

    public static MatrixCursor getEmptyCoursor() {
        return toMatrixCursor(new LinkedList<OnlineTranslation>());
    }
}
