package com.mzagorski.dictFlashcards;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mzagorski.dictFlashcards.dao.DictFlashcardsDbHelper;
import com.mzagorski.dictFlashcards.dao.FlashcardSetDao;
import com.mzagorski.dictFlashcards.listeners.OnSwipeTouchListener;
import com.mzagorski.dictFlashcards.model.Translation;
import com.mzagorski.dictFlashcards.service.FlashcardList;

public class BrowseFlashcardsActivity extends AppCompatActivity {

    public static final String FLASHCARD_SET_ID = "FLASHCARD_SET_ID";
    public static final String REVERSED = "REVERSED";

    private Long flashcardSetId;

    private Boolean isTranslation = false;
    private FlashcardSetDao flashcardSetDao;

    private TextView textView;
    private TextView infoView;
    private Button showButton;

    private FlashcardList list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_flashcards);
        this.flashcardSetId = this.getIntent().getExtras().getLong(FLASHCARD_SET_ID);
        boolean listReversed = this.getIntent().getExtras().getBoolean(REVERSED, false);
        DictFlashcardsDbHelper helper = new DictFlashcardsDbHelper(this);
        SQLiteDatabase db = helper.getWritableDatabase();
        flashcardSetDao = new FlashcardSetDao(db);
        list = new FlashcardList(db, flashcardSetId, listReversed);

        textView = (TextView) findViewById(R.id.flashcard_text);
        infoView = (TextView) findViewById(R.id.flashcard_info);

        showButton = (Button) findViewById(R.id.flashcard_show);
        showButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCurrentRow(!isTranslation);
            }
        });


        final ImageButton prevButton = (ImageButton) findViewById(R.id.flashcard_prev);
        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                movePrev();
            }
        });

        final ImageButton nextButton = (ImageButton) findViewById(R.id.flashcard_next);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveNext();
            }
        });


        ImageButton deleteButton = (ImageButton) findViewById(R.id.flashcard_delete);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flashcardSetDao.deleteFlashcard(flashcardSetId, list.getCurrentRow().id);
                list.remove(list.getCurrentRowIndex());
                if (list.isEmpty()) {
                    close();
                } else {
                    setCurrentRow(false);
                }
            }
        });


        View flashcardView = findViewById(R.id.flashcard_view);
        flashcardView.setOnTouchListener(new OnSwipeTouchListener(this) {

            @Override
            public void onSwipeRight() {
                movePrev();
            }

            @Override
            public void onSwipeLeft() {
                moveNext();
            }

        });

        if (list.isEmpty()) {
            close();
        } else {
            setCurrentRow(false);
        }
    }


    private void setCurrentRow(boolean isShownTranslation) {
        Translation t = list.getCurrentRow();
        textView.setText(isShownTranslation ? t.translation : t.word);
        isTranslation = isShownTranslation;
        showButton.setText(isShownTranslation ? "HIDE" : "SHOW");
        infoView.setText(list.getLangFrom().code + " -> " + list.getLangTo().code + " (" + (list.getCurrentRowIndex() + 1) + " / " + list.size() + ")");
    }


    private void close() {
        new AlertDialog.Builder(this).setTitle("Flashcards empty").setMessage("Flashcards are empty. Add flashcards and try again.")
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        finish();
                    }
                })
                .setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }).show();
    }


    private void moveNext() {
        list.moveNext();
        setCurrentRow(false);
    }


    private void movePrev() {
        list.movePrev();
        setCurrentRow(false);
    }
}
