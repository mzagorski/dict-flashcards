package com.mzagorski.dictFlashcards.service;


import android.database.sqlite.SQLiteDatabase;

import com.mzagorski.dictFlashcards.dao.FlashcardSetDao;
import com.mzagorski.dictFlashcards.dao.LanguageDao;
import com.mzagorski.dictFlashcards.dao.TranslationDao;
import com.mzagorski.dictFlashcards.model.FlashcardSet;
import com.mzagorski.dictFlashcards.model.Language;
import com.mzagorski.dictFlashcards.model.Translation;

import java.util.Set;

public class FlashcardService {

    private final SQLiteDatabase db;
    private final FlashcardSetDao flashcardSetDao;
    private final TranslationDao translationDao;
    private final LanguageDao languageDao;



    public FlashcardService(SQLiteDatabase db) {
        this.db = db;
        this.flashcardSetDao = new FlashcardSetDao(db);
        this.translationDao = new TranslationDao(db);
        this.languageDao = new LanguageDao(db);
    }


    public void addFlashCardToDefaultSet(long translationId) {
        db.beginTransaction();
        try {
            Translation t = translationDao.findById(translationId);
            final FlashcardSet set = flashcardSetDao.findDefault(t.langFrom, t.langTo);
            long setId;
            if(set == null) {
                FlashcardSet newSet = new FlashcardSet();
                newSet.langFrom = t.langFrom;
                newSet.langTo = t.langTo;
                newSet.isDefault = true;
                Language langFrom = languageDao.findById(t.langFrom);
                Language langTo = languageDao.findById(t.langTo);
                newSet.name = langFrom.englishName + " to " + langTo.englishName;
                setId = flashcardSetDao.insert(newSet);
            } else {
                setId = set.id;
            }
            flashcardSetDao.insertFlashcard(setId, translationId);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public void deleteFlashcardFromDefaultSet(long translationId) {
        db.beginTransaction();
        try {
            Translation t = translationDao.findById(translationId);
            final FlashcardSet set = flashcardSetDao.findDefault(t.langFrom, t.langTo);

            flashcardSetDao.deleteFlashcard(set.id, translationId);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }


    public Set<Long> findAllFromDefaultSet(long langFromId, long langToId) {
        return flashcardSetDao.findAllFromDefaultSet(langFromId, langToId);
    }

}
