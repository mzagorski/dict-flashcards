package com.mzagorski.dictFlashcards.service;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.mzagorski.dictFlashcards.dao.FlashcardSetDao;
import com.mzagorski.dictFlashcards.dao.LanguageDao;
import com.mzagorski.dictFlashcards.dao.TranslationDao;
import com.mzagorski.dictFlashcards.model.FlashcardSet;
import com.mzagorski.dictFlashcards.model.Language;
import com.mzagorski.dictFlashcards.model.Translation;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class FlashcardList {

    private final SQLiteDatabase db;
    private final TranslationDao translationDao;
    private final FlashcardSetDao flashcardSetDao;
    private final LanguageDao languageDao;
    private final List<Translation> flashcards = new LinkedList<>();

    private final Language langFrom;
    private final Language langTo;
    private int currentRowIndex;

    public FlashcardList(SQLiteDatabase database, long flashcardSetId, boolean reversed) {
        db = database;
        translationDao = new TranslationDao(db);
        flashcardSetDao = new FlashcardSetDao(db);
        languageDao = new LanguageDao(db);

        FlashcardSet fs = flashcardSetDao.findById(flashcardSetId);
        langFrom = languageDao.findById(fs.langFrom);
        langTo = languageDao.findById(fs.langTo);
        createList(fs, reversed);
    }


    public FlashcardList(SQLiteDatabase database, long flashcardSetId) {
        this(database, flashcardSetId, false);
    }

    public int getCurrentRowIndex() {
        return currentRowIndex;
    }

    public Translation getCurrentRow() {
        return flashcards.get(currentRowIndex);
    }

    public void remove(int index) {
        flashcards.remove(index);
        if(flashcards.isEmpty()) {
            currentRowIndex = 0;
        } else {
            currentRowIndex = currentRowIndex % flashcards.size();
        }
    }

    public boolean isEmpty() {
        return flashcards.isEmpty();
    }

    private void createList(FlashcardSet fs, boolean reversed) {
        Cursor c = translationDao.findAllFromFlashcardSet(fs.id);
        List<Translation> orderedList = new LinkedList<>();
        while (c.moveToNext()) {
            Translation t = translationDao.fromCursorRow(c);
            orderedList.add(reversed ? revers(t) : t);
        }
        c.close();
        Random r = new Random();
        while (!orderedList.isEmpty()) {
            flashcards.add(orderedList.remove(r.nextInt(orderedList.size())));
        }
    }

    private Translation revers(Translation t) {
        Translation r = new Translation();
        r.id = t.id;
        r.langFrom = t.langTo;
        r.langTo = t.langFrom;
        r.translation = t.word;
        r.word = t.translation;
        return r;
    }

    public Language getLangFrom() {
        return langFrom;
    }

    public Language getLangTo() {
        return langTo;
    }

    public int size() {
        return flashcards.size();
    }

    public void moveNext() {
        this.currentRowIndex = (currentRowIndex + 1) % this.flashcards.size();
    }


    public void movePrev() {
        int index = (currentRowIndex - 1) % this.flashcards.size();
        if (index < 0) {
            index = this.flashcards.size() - 1;
        }
        this.currentRowIndex = index;
    }

    public List<Translation> getFlashcards() {
        return flashcards;
    }
}
