package com.mzagorski.dictFlashcards.fragments;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.mzagorski.dictFlashcards.EditTranslationActivity;
import com.mzagorski.dictFlashcards.R;
import com.mzagorski.dictFlashcards.adapters.DictListAdapter;
import com.mzagorski.dictFlashcards.adapters.OnlineDictListAdapter;
import com.mzagorski.dictFlashcards.dao.DictFlashcardsDbHelper;
import com.mzagorski.dictFlashcards.dao.LanguageDao;
import com.mzagorski.dictFlashcards.dao.SettingsDao;
import com.mzagorski.dictFlashcards.dao.TranslationDao;
import com.mzagorski.dictFlashcards.httpClient.GlosbeHttpClient;
import com.mzagorski.dictFlashcards.httpClient.OnlineTranslation;
import com.mzagorski.dictFlashcards.model.Language;
import com.mzagorski.dictFlashcards.model.Setting;
import com.mzagorski.dictFlashcards.model.Translation;

import org.json.JSONException;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;


public class BrowseDictFragment extends Fragment {

    private List<Language> langsFrom;
    private List<Language> langsTo;
    private Spinner langsToSpinner;
    private Spinner langsFromSpinner;
    private LanguageDao languageDao;
    private SettingsDao settingsDao;
    private String selectedText;
    private DictListAdapter listAdapter;
    private ListView listview;
    private ListView onlineListView;
    private Button findOnlineButton;
    private Button addButton;
    private GlosbeHttpClient remoteTranslClient = new GlosbeHttpClient();
    private OnlineDictListAdapter onlineListAdapter;
    private LinearLayout buttonsLayout;
    private ProgressBar onlineSearchProgressBar;
    private Button swapLangsButton;


    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static BrowseDictFragment newInstance() {
        BrowseDictFragment fragment = new BrowseDictFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        DictFlashcardsDbHelper helper  = new DictFlashcardsDbHelper(getActivity());
        final SQLiteDatabase db = helper.getWritableDatabase();
        final TranslationDao translationDao = new TranslationDao(db);
        languageDao = new LanguageDao(db);
        settingsDao = new SettingsDao(db);

        View rootView = inflater.inflate(R.layout.browse_dict_fragment, container, false);
        listview = (ListView)rootView.findViewById(R.id.dict_list);
        buttonsLayout = (LinearLayout)rootView.findViewById(R.id.dict_buttons_layout);
        findOnlineButton = (Button)rootView.findViewById(R.id.dict_find_online_button);
        onlineListView = (ListView)rootView.findViewById(R.id.online_dict_list);
        onlineSearchProgressBar = (ProgressBar)rootView.findViewById(R.id.online_dict_list_progress);
        onlineListAdapter = new OnlineDictListAdapter(getActivity(), GlosbeHttpClient.toMatrixCursor(new LinkedList<OnlineTranslation>()), 0);
        onlineListView.setAdapter(onlineListAdapter);
        findOnlineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Language langFrom = languageDao.findFromSettings(Setting.KEY_TRANS_LANG_FROM);
                final Language langTo = languageDao.findFromSettings(Setting.KEY_TRANS_LANG_TO);
                try {
                    listview.setVisibility(View.GONE);
                    buttonsLayout.setVisibility(View.GONE);
                    onlineSearchProgressBar.setVisibility(View.VISIBLE);
                    List<OnlineTranslation> foundTranslations = remoteTranslClient.translate(langFrom, langTo, selectedText);
                    onlineListAdapter.changeCursor(GlosbeHttpClient.toMatrixCursor(foundTranslations));
                    if (foundTranslations.isEmpty()) {
                        Toast.makeText(BrowseDictFragment.this.getContext(), "No translations found.", Toast.LENGTH_LONG).show();
                        BrowseDictFragment.this.reloadList();
                    }
                } catch (IOException e) {
                    Log.e("on-line-tran-error", "on-line-translation-error", e);
                    Toast.makeText(BrowseDictFragment.this.getContext(), "Cannot connect to the server. Please check your internet connection.", Toast.LENGTH_LONG).show();
                    BrowseDictFragment.this.reloadList();
                } catch (JSONException e) {
                    Log.e("on-line-tran-json-error", "on-line-translation-json-error", e);
                    Toast.makeText(BrowseDictFragment.this.getContext(), "Unexpected exception - please contact support team", Toast.LENGTH_LONG).show();
                    BrowseDictFragment.this.reloadList();
                } finally {
                    onlineSearchProgressBar.setVisibility(View.GONE);
                }
            }
        });
        addButton = (Button)rootView.findViewById(R.id.dict_add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity activity = BrowseDictFragment.this.getActivity();
                Translation t = new Translation();
                t.word = selectedText;
                Intent intent = new Intent(activity, EditTranslationActivity.class);
                intent.putExtra(EditTranslationActivity.TRANSLATION, t);
                activity.startActivityForResult(intent, DictListAdapter.RESULT_LIST_MODIFIED);
            }
        });

        swapLangsButton = (Button)rootView.findViewById(R.id.dict_list_swap_langs_button);
        swapLangsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsDao.swapLangauges();
                refreshLangsFromSpinner();
                refreshLangsToSpinner();
                reloadList();
            }
        });

        listAdapter = new DictListAdapter(getActivity(), translationDao.findWordsInSelectedLang(null), 0);
        listview.setAdapter(listAdapter);
        EditText textMessage = (EditText)rootView.findViewById(R.id.dict_search_input);
        textMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                selectedText = s.toString();
                reloadList(selectedText);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        langsFromSpinner = (Spinner)rootView.findViewById(R.id.dict_lang_from);
        refreshLangsFromSpinner();

        langsToSpinner = (Spinner)rootView.findViewById(R.id.dict_lang_to);
        refreshLangsToSpinner();

        langsFromSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            private int previousSelection = -1;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(previousSelection == position) {
                    return;
                }
                previousSelection = position;
                settingsDao.saveSetting(Setting.KEY_TRANS_LANG_FROM, langsFrom.get(position).id.toString());
                refreshLangsToSpinner();
                reloadList(selectedText);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        langsToSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            private int previousSelection = -1;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(previousSelection == position) {
                    return;
                }
                previousSelection = position;
                settingsDao.saveSetting(Setting.KEY_TRANS_LANG_TO, langsTo.get(position).id.toString());
                refreshLangsFromSpinner();
                reloadList(selectedText);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return rootView;
    }


    private void refreshLangsFromSpinner() {
        langsFrom = languageDao.selectAll();
        final Language langFrom = languageDao.findFromSettings(Setting.KEY_TRANS_LANG_FROM);
        int selected = langsFrom.indexOf(langFrom);
        ArrayAdapter<String> langsFromAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, langsToArray(langsFrom));
        langsFromSpinner.setAdapter(langsFromAdapter);
        langsFromSpinner.setSelection(selected);
    }

    private void refreshLangsToSpinner() {
        langsTo = languageDao.selectAll();
        final Language langTo = languageDao.findFromSettings(Setting.KEY_TRANS_LANG_TO);
        int selected = langsTo.indexOf(langTo);
        ArrayAdapter<String> langsToAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, langsToArray(langsTo));
        langsToSpinner.setAdapter(langsToAdapter);
        langsToSpinner.setSelection(selected);
    }


    private String[] langsToArray(List<Language> langs) {
        String[] arr = new String[langs.size()];
        for(int i = 0; i< langs.size(); i++) {
            arr[i] = langs.get(i).englishName;
        }
        return arr;
    }


    private void reloadList(String filter) {
        onlineListAdapter.changeCursor(GlosbeHttpClient.getEmptyCoursor());
        int rowsFound = listAdapter.reloadCursor(filter);
        if(rowsFound == 0) {
            listview.setVisibility(View.GONE);
            buttonsLayout.setVisibility(View.VISIBLE);
            onlineListView.setVisibility(View.VISIBLE);
        } else {
            listview.setVisibility(View.VISIBLE);
            buttonsLayout.setVisibility(View.GONE);
            onlineListView.setVisibility(View.GONE);
        }
    }


    public void reloadList() {
        reloadList(selectedText);
    }
}
