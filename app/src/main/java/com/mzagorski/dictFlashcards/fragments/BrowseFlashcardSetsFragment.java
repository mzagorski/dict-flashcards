package com.mzagorski.dictFlashcards.fragments;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mzagorski.dictFlashcards.BrowseFlashcardsActivity;
import com.mzagorski.dictFlashcards.R;
import com.mzagorski.dictFlashcards.adapters.FlashCardSetListAdapter;
import com.mzagorski.dictFlashcards.dao.DictFlashcardsDbHelper;
import com.mzagorski.dictFlashcards.dao.FlashcardSetDao;


public class BrowseFlashcardSetsFragment extends Fragment {


    public BrowseFlashcardSetsFragment() {
    }


    public static BrowseFlashcardSetsFragment newInstance() {
        BrowseFlashcardSetsFragment fragment = new BrowseFlashcardSetsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.browse_flashcard_sets_fragment, container, false);
        ListView listview = (ListView) rootView.findViewById(R.id.flashcard_set_list);
        DictFlashcardsDbHelper helper = new DictFlashcardsDbHelper(getActivity());
        final SQLiteDatabase db = helper.getWritableDatabase();
        final FlashcardSetDao flashcardSetDao = new FlashcardSetDao(db);
        FlashCardSetListAdapter adapter = new FlashCardSetListAdapter(getActivity(), flashcardSetDao.getList(), 0);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), BrowseFlashcardsActivity.class);
                intent.putExtra(BrowseFlashcardsActivity.FLASHCARD_SET_ID, id);
                startActivity(intent);
            }
        });
        return rootView;
    }
}
