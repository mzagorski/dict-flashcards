package com.mzagorski.dictFlashcards;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.mzagorski.dictFlashcards.dao.DictFlashcardsDbHelper;
import com.mzagorski.dictFlashcards.dao.FlashcardSetDao;
import com.mzagorski.dictFlashcards.dao.LanguageDao;
import com.mzagorski.dictFlashcards.dao.TranslationDao;
import com.mzagorski.dictFlashcards.model.FlashcardSet;
import com.mzagorski.dictFlashcards.model.Language;
import com.mzagorski.dictFlashcards.model.Translation;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class ImportFlashcardSetActivity extends AppCompatActivity {

    private List<FileContent> content;

    private LanguageDao languageDao;
    private TranslationDao translationDao;
    private FlashcardSetDao flashcardSetDao;
    private List<Language> langs;

    Spinner langsFromSpinner;
    Spinner langsToSpinner;
    Button saveButton;

    private EditText setName;

    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_import_flashcard_set);
        startFileDialog();

        DictFlashcardsDbHelper helper  = new DictFlashcardsDbHelper(this);
        db = helper.getWritableDatabase();
        languageDao = new LanguageDao(db);
        translationDao = new TranslationDao(db);
        flashcardSetDao = new FlashcardSetDao(db);
        langs = languageDao.selectAll();
        langsFromSpinner = (Spinner)findViewById(R.id.import_fc_lang_from);
        setLangsAdapter(langsFromSpinner);
        langsToSpinner = (Spinner)findViewById(R.id.import_fc_lang_to);
        setLangsAdapter(langsToSpinner);

        setName = (EditText)findViewById(R.id.import_fc_set_name);
        saveButton = (Button)findViewById(R.id.import_fc_save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveFlashcardSet();
            }
        });
    }

    private void setLangsAdapter(Spinner spinner) {
        ArrayAdapter<String> langsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, langsToArray(langs));
        spinner.setAdapter(langsAdapter);
    }

    private String[] langsToArray(List<Language> langs) {
        String[] arr = new String[langs.size()];
        for(int i = 0; i< langs.size(); i++) {
            arr[i] = langs.get(i).englishName;
        }
        return arr;
    }

    private void startFileDialog() {
        Intent intent = new Intent().setType("text/*").setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select a file"), 123);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 123 && resultCode == RESULT_OK) {
            try {
                content = parseLines(readFile(data.getData()));
                detectLanguages();
            } catch (Exception e) {
                Log.e("file-not-read", "file-not-read", e);
                showErrorDialog("Error", "File cannot be read", true);
            }
        } else {
            finish();
        }
    }

    private boolean validId(Long id) {
        return id != null && id > 0;
    }

    private void showErrorDialog(String title, String msg) {
        showErrorDialog(title, msg, false);
    }

    private void showErrorDialog(String title, String msg, final boolean finishActivityOnExit) {
        new AlertDialog.Builder(ImportFlashcardSetActivity.this)
                .setTitle(title).setMessage(msg)
                .setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        if(finishActivityOnExit) {
                            finish();
                        }
                    }
                }).show();
    }

    private void saveFlashcardSet() {
        Long langFromId = langs.get((int) langsFromSpinner.getSelectedItemId()).id;
        Long langToId = langs.get((int) langsToSpinner.getSelectedItemId()).id;
        String name = setName.getText().toString().trim();
        boolean savedSuccessfully = false;
        if(content.isEmpty()) {
            showErrorDialog("Error", "File content is empty", true);
        }
        if(validId(langFromId) && validId(langToId) && !StringUtils.isBlank(name)) {
            try {
                db.beginTransaction();
                List<Translation> translations = findOrInsertTranslations(langFromId, langToId, content);
                FlashcardSet fs = flashcardSetDao.findByName(name);
                if(fs != null) {
                    if(fs.langFrom != langFromId || fs.langTo != langToId) {
                        throw new Exception("Set already exists in different language");
                    }
                    mergeFlashcardSets(fs, translations);
                } else {
                    insertFlashcardSet(name, langFromId, langToId, translations);
                }
                db.setTransactionSuccessful();
                savedSuccessfully = true;
            } catch (Throwable e) {
                showErrorDialog("Flashcard set cannot be saved", "Please check if not exist any set with the same name");
            } finally {
                db.endTransaction();
            }
        } else {
            showErrorDialog("Flashcard set cannot be saved", "Languages not selected or name is empty");
        }
        if(savedSuccessfully) {
            finish();
        }
    }

    private List<String> readFile(Uri uri) throws IOException {
        List<String> lines = new LinkedList<>();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(getContentResolver().openInputStream(uri)));
            String line;
            while ((line = br.readLine()) != null) {
                if (StringUtils.isNotBlank(line)) {
                    lines.add(line.trim());
                }
            }
        } finally {
             try {
                 br.close();
             } catch (Exception e) {
                 Log.e("buffer-not-closed", "buffer-not-closed", e);
             }
        }
        return lines;
    }

    private List<FileContent> parseLines(List<String> lines) {
        List<FileContent> contents = new ArrayList<>(lines.size());
        for(String line: lines) {
            String[] l = line.split("\\-");
            if(l.length != 2) {
                throw new RuntimeException("Illegal file format");
            }
            contents.add(new FileContent(l[0].trim(), l[1].trim()));
        }
        return contents;
    }

    private List<Translation> findOrInsertTranslations(Long fromId, Long toId, List<FileContent> content) {
        List<Translation> l = new ArrayList<>(content.size());
        for(FileContent c: content) {
            List<Translation> found = translationDao.find(c.from, c.to, fromId, toId);
            Translation t;
            if(found.isEmpty()) {
                t = new Translation();
                t.word = c.from;
                t.translation = c.to;
                t.langFrom = fromId;
                t.langTo = toId;
                t.id = translationDao.insert(t);
            } else {
                t = found.get(0);
            }
            l.add(t);
        }
        return l;
    }

    private void mergeFlashcardSets(FlashcardSet fs, List<Translation> translations) {
        Set<Long> existingTranslations = flashcardSetDao.findTranslationIds(fs.id);
        for(Translation t: translations) {
            if(!existingTranslations.contains(t.id)) {
                flashcardSetDao.insertFlashcard(fs.id, t.id);
            }
        }
    }


    private void detectLanguages() {
        List<FileContent> toCheck = rand(content, 7);
        Map<Long, Integer> fromMax = new HashMap<>();
        Map<Long, Integer> toMax = new HashMap<>();
        for(Language l : langs) {
            fromMax.put(l.id, 0);
            toMax.put(l.id, 0);
        }
        for(FileContent f: toCheck) {
            for(Translation t: translationDao.findByWord(f.from)) {
                fromMax.put(t.langFrom, fromMax.get(t.langFrom) + 1);
            }
            for(Translation t: translationDao.findByTransl(f.from)) {
                fromMax.put(t.langTo, fromMax.get(t.langTo) + 1);
            }
            for(Translation t: translationDao.findByTransl(f.to)) {
                toMax.put(t.langTo, toMax.get(t.langTo) + 1);
            }
            for(Translation t: translationDao.findByWord(f.to)) {
                toMax.put(t.langFrom, toMax.get(t.langFrom) + 1);
            }
        }
        Long fromM = maxFromMap(fromMax);
        Long toM = maxFromMap(toMax);
        selectLanguage(langsFromSpinner, maxFromMap(fromMax));
        selectLanguage(langsToSpinner, maxFromMap(toMax));
    }

    private void selectLanguage(Spinner spinner, Long langId) {
        if(langId == null || langId <= 0) {
            return;
        }
        int index = -1;
        for(int i = 0; i< langs.size(); i++) {
            if(langs.get(i).id == langId) {
                index = i;
                break;
            }
        }
        if(index >= 0) {
            spinner.setSelection(index);
        }
    }

    private Long maxFromMap(Map<Long, Integer> m) {
        Long maxId = -1L;
        int maxVal = 0;
        for(Map.Entry<Long, Integer> e: m.entrySet()) {
            if(e.getValue() > maxVal) {
                maxVal = e.getValue();
                maxId = e.getKey();
            }
        }
        return maxId > -1 ? maxId : null;
    }

    private <T> List<T> rand(List<T> l, int amount) {

        if(amount > l.size()) {
            return l;
        }
        Random r = new Random();
        Set<Integer> ids = new HashSet<>();
        while(ids.size() < amount) {
            ids.add(r.nextInt(l.size()));
        }
        List<T> subL = new LinkedList<>();
        for(Integer i: ids) {
            subL.add(l.get(i));
        }
        return subL;
    }


    private void insertFlashcardSet(String name, long from, long to, List<Translation> translations) {
        FlashcardSet fs = new FlashcardSet();
        fs.name = name;
        fs.langFrom = from;
        fs.langTo = to;
        fs.id = flashcardSetDao.insert(fs);
        if(fs.id <= 0) {
            throw new RuntimeException("flashcard set not saved correctly");
        }
        for(Translation t: translations) {
            flashcardSetDao.insertFlashcard(fs.id, t.id);
        }
    }

    class FileContent {
        final String from;
        final String to;

        public FileContent(String from, String to) {
            this.from = from;
            this.to = to;
        }
    }
}
