package com.mzagorski.dictFlashcards.dao;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.mzagorski.dictFlashcards.model.Translation;

import org.apache.commons.lang3.StringUtils;

import java.util.LinkedList;
import java.util.List;


public class TranslationDao {

    private final SQLiteDatabase db;
    private final String TABLE_NAME = "translation";

    private final static String selectQuery = "select translation.id, translation.word, translation.translation, " +
            "translation.lang_from, translation.lang_to from translation";

    public TranslationDao(SQLiteDatabase db) {
        this.db = db;
    }





    public Cursor buildFindWordsInSelectedLangConditions(String selectStatement, String filter) {
        final String where = " lang_from = (select CAST(value as BIGINT) from settings where key = 'TRANS_LANG_FROM') AND" +
                " lang_to = (select CAST(value as BIGINT) from settings where key = 'TRANS_LANG_TO')";
        final String noArgsQuery = selectStatement + where;
        Cursor c;
        String order = " ORDER BY word_ascii";
        if(filter == null || filter.trim().isEmpty()) {
            c = db.rawQuery(noArgsQuery + order, new String[]{});
        } else {
            c = db.rawQuery(noArgsQuery + " AND word_ascii LIKE ? COLLATE NOCASE" + order, new String[]{  filter.trim() + "%" });
        }
        return c;
    }


    public int countWordsInSelectedLang(String filter) {
        final String noArgsQuery = "SELECT count(*) FROM translation WHERE ";
        Cursor c = buildFindWordsInSelectedLangConditions(noArgsQuery, toLowerCaseAscii(filter));
        c.moveToNext();
        int count = c.getInt(0);
        c.close();
        return count;
    }


    public Cursor findWordsInSelectedLang(String filter) {
        final String noArgsQuery = "select translation.word as word, translation.translation as translation, default_flashcards.translation_id as has_flashcard," +
                "   translation.id as _id FROM translation " +
                " LEFT OUTER JOIN " +
                " (SELECT flashcard.translation_id as translation_id FROM flashcard " +
                " INNER JOIN flashcard_set ON flashcard_set.id = flashcard.set_id" +
                " WHERE flashcard_set.lang_from = (select CAST(value as BIGINT) from settings where key = 'TRANS_LANG_FROM') AND" +
                " flashcard_set.lang_to = (select CAST(value as BIGINT) from settings where key = 'TRANS_LANG_TO') AND " +
                " flashcard_set.is_default <> 0) default_flashcards " +
                " ON default_flashcards.translation_id = translation.id " +
                " WHERE ";
        return buildFindWordsInSelectedLangConditions(noArgsQuery, toLowerCaseAscii(filter));
    }


    public Translation findById(long id) {
        Cursor c = db.rawQuery(selectQuery + " WHERE id = ?", new String[]{String.valueOf(id)});
        if(!c.moveToFirst()) {
            c.close();
            return null;
        }
        Translation t = fromCursorRow(c);
        c.close();
        return t;
    }

    public List<Translation> find(String word, String transl, long from, long to) {
        Cursor c = db.rawQuery(selectQuery + " WHERE word = ? AND translation = ? AND lang_from = ? AND lang_to = ?",
                new String[]{word, transl, String.valueOf(from), String.valueOf(to)});
        List<Translation> resList = new LinkedList<>();
        while(c.moveToNext()) {
            resList.add(fromCursorRow(c));
        }
        c.close();
        return resList;
    }


    public Translation fromCursorRow(Cursor c) {
        Translation t = new Translation();
        int i = 0;
        t.id = c.getLong(i++);
        t.word = c.getString(i++);
        t.translation = c.getString(i++);
        t.langFrom = c.getLong(i++);
        t.langTo = c.getLong(i++);
        return t;
    }


    public void delete(Long translationId) {
        db.beginTransaction();
        try {
            db.delete(TABLE_NAME, "id = ?", new String[]{translationId.toString()});
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }


    public Cursor findAllFromFlashcardSet(long setId) {
        return db.rawQuery(selectQuery + " INNER JOIN flashcard ON flashcard.translation_id = translation.id " +
                        " WHERE flashcard.set_id = ? ",
                new String[]{String.valueOf(setId)});
    }


    static String toLowerCaseAscii(String utfString) {
        return utfString == null ? null : StringUtils.stripAccents(utfString).toLowerCase().replaceAll("ł", "l").trim();
    }


    public Long insert(Translation translation) {
        ContentValues cv = getContent(translation);
        cv.put("word_ascii", toLowerCaseAscii(translation.word));
        return db.insert(TABLE_NAME, null, cv);
    }


    public void update(Translation translation) {
        ContentValues cv = getContent(translation);
        db.update(TABLE_NAME, cv, "id = ?", new String[]{String.valueOf(translation.id)});
    }


    private ContentValues getContent(Translation t) {
        ContentValues cv = new ContentValues();
        cv.put("word", t.word);
        cv.put("translation", t.translation);
        cv.put("lang_from", t.langFrom);
        cv.put("lang_to", t.langTo);
        return cv;
    }

    public List<Translation> findByWord(String word) {
        Cursor c = db.rawQuery(selectQuery + " WHERE word_ascii = ? ",
                new String[]{toLowerCaseAscii(word)});
        List<Translation> resList = new LinkedList<>();
        while(c.moveToNext()) {
            resList.add(fromCursorRow(c));
        }
        c.close();
        return resList;
    }

    public List<Translation> findByTransl(String transl) {
        Cursor c = db.rawQuery(selectQuery + " WHERE translation = ? ",
                new String[]{transl});
        List<Translation> resList = new LinkedList<>();
        while(c.moveToNext()) {
            resList.add(fromCursorRow(c));
        }
        c.close();
        return resList;
    }
}
