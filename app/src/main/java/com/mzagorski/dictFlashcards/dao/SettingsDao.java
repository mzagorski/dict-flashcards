package com.mzagorski.dictFlashcards.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.mzagorski.dictFlashcards.model.Setting;


public class SettingsDao {

    private final SQLiteDatabase db;

    public SettingsDao(SQLiteDatabase db) {
        this.db = db;
    }


    public void saveSetting(final String key, final String value) {
        try {
            db.beginTransaction();
            Cursor c = db.rawQuery("select count(*) from settings where key = ?", new String[]{key});
            c.moveToFirst();
            if (c.getInt(0) == 0) {
                insertSetting(key, value);
            } else {
                updateSetting(key, value);
            }
            c.close();
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }


    private void insertSetting(final String key, final String value) {
        db.insert("settings", null, getContentValues(key, value));
    }

    private void updateSetting(final String key, final String value) {
        db.update("settings", getContentValues(key, value), "key = ?", new String[]{key});
    }


    private ContentValues getContentValues(final String key, final String value) {
        ContentValues cv = new ContentValues();
        cv.put("key", key);
        cv.put("value", value);
        return cv;
    }


    private String getSetting(String key) {
        Cursor c = db.rawQuery("select value from settings where key = ?", new String[]{key});
        c.moveToNext();
        String value = c.getString(0);
        c.close();
        return value;
    }


    public void swapLangauges() {
        db.beginTransaction();
        try {
            final String oldLangFrom = getSetting(Setting.KEY_TRANS_LANG_FROM);
            final String oldLangTo = getSetting(Setting.KEY_TRANS_LANG_TO);
            updateSetting(Setting.KEY_TRANS_LANG_FROM, oldLangTo);
            updateSetting(Setting.KEY_TRANS_LANG_TO, oldLangFrom);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }
}
