package com.mzagorski.dictFlashcards.dao;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.mzagorski.dictFlashcards.model.Setting;

import java.io.IOException;
import java.io.InputStream;

public class DictFlashcardsDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 3;

    public static final String DATABASE_NAME = "flashcard_dict.db";

    private final Context context;

    public DictFlashcardsDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(readAssetsTextFile("database/create_language_table.sql"));
            db.execSQL(readAssetsTextFile("database/create_translation_table.sql"));
            db.execSQL(readAssetsTextFile("database/create_settings_table.sql"));
            db.execSQL(readAssetsTextFile("database/create_flashcard_set_table.sql"));
            db.execSQL(readAssetsTextFile("database/create_flashcard_table.sql"));
            insertLanguages(db);
            Long esId = findLangId("es", db);
            Long enId = findLangId("en", db);
            Long plId = findLangId("pl", db);
            Long deId = findLangId("de", db);
            insertTranslations(db, "database/polish_spanish.txt", plId, esId);
            insertTranslations(db, "database/polish_english.txt", plId, enId);
            insertTranslations(db, "database/polish_german.txt", plId, deId);
            insertTranslations(db, "database/german_polish.txt", deId, plId);
            insertTranslations(db, "database/english_polish.txt", enId, plId);
            insertTranslations(db, "database/spanish_polish.txt", esId, plId);
            initSettings(db);
            version2Upgrades(db);
            version3Upgrades(db);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private Long findLangId(String code, SQLiteDatabase db) {
        Cursor c = db.rawQuery("select id from language where code = ?", new String[]{code});
        c.moveToNext();
        Long id = c.getLong(0);
        c.close();
        return id;
    }


    private void initSettings(SQLiteDatabase db) {
        SettingsDao dao = new SettingsDao(db);
        dao.saveSetting(Setting.KEY_TRANS_LANG_FROM, findLangId("pl", db).toString());
        dao.saveSetting(Setting.KEY_TRANS_LANG_TO, findLangId("es", db).toString());
    }


    private void insertLanguages(final SQLiteDatabase db) {
        db.insert("language", null, getLangContentValues("en", "English"));
        db.insert("language", null, getLangContentValues("es", "Spanish"));
        db.insert("language", null, getLangContentValues("pl", "Polish"));
        db.insert("language", null, getLangContentValues("de", "German"));
    }


    private ContentValues getLangContentValues(final String code, final String englishName) {
        final ContentValues vals = new ContentValues();
        vals.put("code", code);
        vals.put("english_name", englishName);
        return vals;
    }


    private void insertTranslations(final SQLiteDatabase db, final String filePath, final long langFrom, final long langTo) {
        db.beginTransaction();
        try {
            for(String line: readAssetsTextFile(filePath).split("\n")) {
                String[] splittedLine = splitDictLine(line);
                final ContentValues vals = new ContentValues();
                vals.put("word", splittedLine[0]);
                vals.put("translation", splittedLine[1]);
                vals.put("lang_from", langFrom);
                vals.put("lang_to", langTo);
                db.insert("translation", null, vals);
            }
            db.setTransactionSuccessful();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
    }


    private String[] splitDictLine(String line) {
        String[] result = new String[2];
        String[] splittedLine = line.split("-");
        StringBuilder strBuilder = new StringBuilder();
        for (int i = 1; i < splittedLine.length; i++) {
            strBuilder.append(splittedLine[i]);
            if(i < splittedLine.length -1) {
                strBuilder.append("-");
            }
        }
        result[0] = splittedLine[0];
        result[1] = strBuilder.toString();
        return result;
    }



    private String readAssetsTextFile(final String filePath) throws IOException {
        InputStream inputStream = null;
        try {
            inputStream = context.getAssets().open(filePath);
            int n = inputStream.available();
            byte[] bytes = new byte[n];
            inputStream.read(bytes, 0, n);
            return new String(bytes, "UTF-8");
        } finally {
            if(inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) { }
            }
        }
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(newVersion >= 2 && oldVersion < 2) {
            version2Upgrades(db);
        }
        if(newVersion >= 3 && oldVersion < 3) {
            version3Upgrades(db);
        }
    }


    private void version2Upgrades(SQLiteDatabase db) {
        db.execSQL("ALTER TABLE language ADD COLUMN iso_639_3_code varchar(3)"); // TODO should be not null
        db.execSQL("UPDATE language SET iso_639_3_code = 'eng' where code = 'en'");
        db.execSQL("UPDATE language SET iso_639_3_code = 'spa' where code = 'es'");
        db.execSQL("UPDATE language SET iso_639_3_code = 'deu' where code = 'de'");
        db.execSQL("UPDATE language SET iso_639_3_code = 'pol' where code = 'pl'");
    }


    private void version3Upgrades(SQLiteDatabase db) {
        db.execSQL("ALTER TABLE translation ADD COLUMN word_ascii varchar(500)"); // TODO should be not null
        db.beginTransaction();
        try {
            Cursor c = db.rawQuery("select id, word from translation", new String[0]);
            while(c.moveToNext()) {
                String id = c.getString(0);
                String word = c.getString(1);
                ContentValues cv = new ContentValues();
                cv.put("word_ascii", TranslationDao.toLowerCaseAscii(word));
                db.update("translation", cv, "id = ?", new String[]{id});
            }
            c.close();
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }
}
