package com.mzagorski.dictFlashcards.dao;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.mzagorski.dictFlashcards.model.FlashcardSet;

import java.util.HashSet;
import java.util.Set;

public class FlashcardSetDao {

    private final SQLiteDatabase db;

    public FlashcardSetDao(SQLiteDatabase db) {
        this.db = db;
    }

    private final String SELECT_QUERY = "select id, name, lang_from, lang_to, is_default from flashcard_set";


    public Cursor getList() {
        return db.rawQuery("SELECT DISTINCT flashcard_set.name as name, flashcard_set.id as _id FROM flashcard_set " +
                " INNER JOIN flashcard ON flashcard.set_id = flashcard_set.id " +
                " ORDER BY name", new String[]{});
    }


    public FlashcardSet findDefault(long langFromId, long langToId) {
        Cursor c = db.rawQuery(SELECT_QUERY + " WHERE is_default <> 0 " +
                " AND lang_from = ? AND lang_to = ?", new String[]{String.valueOf(langFromId), String.valueOf(langToId)});
        if(!c.moveToFirst()) {
            c.close();
            return null;
        }
        FlashcardSet flashSet = fromCursor(c);
        c.close();
        return flashSet;
    }

    public FlashcardSet findByName(final String name) {
        Cursor c = db.rawQuery(SELECT_QUERY + " WHERE name = ? ", new String[]{name});
        if(!c.moveToFirst()) {
            c.close();
            return null;
        }
        FlashcardSet flashSet = fromCursor(c);
        c.close();
        return flashSet;
    }

    public FlashcardSet findById(final Long id) {
        Cursor c = db.rawQuery(SELECT_QUERY + " WHERE id = ? ", new String[]{String.valueOf(id)});
        if(!c.moveToFirst()) {
            c.close();
            return null;
        }
        FlashcardSet flashSet = fromCursor(c);
        c.close();
        return flashSet;
    }

    public Set<Long> findTranslationIds(final long setId) {
        Cursor c = db.rawQuery("SELECT translation_id FROM flashcard WHERE set_id = ? ", new String[]{String.valueOf(setId)});
        Set<Long> r = new HashSet<>();
        while(c.moveToNext()) {
            r.add(c.getLong(0));
        }
        c.close();
        return r;
    }

    public FlashcardSet fromCursor(Cursor c) {
        FlashcardSet flashSet = new FlashcardSet();
        int i = 0;
        flashSet.id = c.getLong(i++);
        flashSet.name = c.getString(i++);
        flashSet.langFrom = c.getLong(i++);
        flashSet.langTo = c.getLong(i++);
        flashSet.isDefault = 0 != c.getInt(i++);
        return flashSet;
    }


    public long insert(FlashcardSet f) {
        final ContentValues vals = new ContentValues();
        vals.put("name", f.name);
        vals.put("lang_from", f.langFrom);
        vals.put("lang_to", f.langTo);
        vals.put("is_default", f.isDefault ? 1 : 0);
        return db.insert("flashcard_set", null, vals);
    }


    public long insertFlashcard(long setId, long translationId) {
        final ContentValues vals = new ContentValues();
        vals.put("set_id", setId);
        vals.put("translation_id", translationId);
        return db.insert("flashcard", null, vals);
    }


    public int deleteFlashcard(long setId, long translationId) {
        return db.delete("flashcard", "set_id = ? AND translation_id = ?", new String[]{String.valueOf(setId), String.valueOf(translationId)});
    }


    public Set<Long> findAllFromDefaultSet(long langFromId, long langToId) {
        Cursor c = db.rawQuery("select flashcard.id FROM flashcard_set " +
                " INNER JOIN flashcard ON flashcard_set.id = flashcard.set_id" +
                " WHERE flashcard_set.is_default <> 0 " +
                " AND flashcard_set.lang_from = ? AND flashcard_set.lang_to = ?", new String[]{String.valueOf(langFromId), String.valueOf(langToId)});
        Set<Long> ids = new HashSet<>();
        while(c.moveToNext()) {
            ids.add(c.getLong(0));
        }
        c.close();
        return ids;
    }

}
