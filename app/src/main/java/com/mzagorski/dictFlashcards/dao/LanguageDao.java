package com.mzagorski.dictFlashcards.dao;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.mzagorski.dictFlashcards.model.Language;

import java.util.LinkedList;
import java.util.List;

public class LanguageDao {

    private final SQLiteDatabase db;

    private static final String SELECT = "select id, code, english_name, iso_639_3_code from language ";

    public LanguageDao(SQLiteDatabase db) {
        this.db = db;
    }


    public Language findByCode(String code) {
        Cursor c = db.rawQuery(SELECT + " WHERE code = ?", new String[]{code});
        Language l = toList(c).get(0);
        c.close();
        return l;
    }

    public Language findById(long id) {
        Cursor c = db.rawQuery(SELECT + " WHERE id = ?", new String[]{String.valueOf(id)});
        Language l = toList(c).get(0);
        c.close();
        return l;
    }


    public Language findFromSettings(String key) {
        Cursor c = db.rawQuery(SELECT + " WHERE id = (select CAST(value as BIGINT) from settings where key = ?)", new String[]{key});
        Language l = toList(c).get(0);
        c.close();
        return l;
    }


    public List<Language> selectAll() {
        Cursor c = db.rawQuery(SELECT, new String[]{});
        List<Language> result = toList(c);
        c.close();
        return result;
    }


    private List<Language> toList(Cursor c) {
        List<Language> langs = new LinkedList<>();
        while(c.moveToNext()) {
            langs.add(new Language(c.getLong(0), c.getString(1), c.getString(2), c.getString(3)));
        }
        return langs;
    }
}
