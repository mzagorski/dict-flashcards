package com.mzagorski.dictFlashcards.adapters;


import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.mzagorski.dictFlashcards.R;
import com.mzagorski.dictFlashcards.model.Translation;

import java.util.List;

public class FlashcardListAdapter extends CursorAdapter {

    public FlashcardListAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.flashcard_list_row, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView word = (TextView) view.findViewById(R.id.flashcards_list_row_text);
        word.setText(cursor.getString(1));
        TextView translation = (TextView) view.findViewById(R.id.flashcards_list_row_translation);
        translation.setText(cursor.getString(2));
    }

    public static Cursor toCursor(List<Translation> translations) {
        MatrixCursor matrixCursor = new MatrixCursor(new String[]{"_id", "word", "translation"});
        for (Translation t : translations) {
            matrixCursor.addRow(new Object[]{t.id, t.word, t.translation});
        }
        return matrixCursor;
    }
}
