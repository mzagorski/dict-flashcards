package com.mzagorski.dictFlashcards.adapters;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mzagorski.dictFlashcards.BrowseFlashcardsActivity;
import com.mzagorski.dictFlashcards.BrowseFlashcardsListActivity;
import com.mzagorski.dictFlashcards.R;


public class FlashCardSetListAdapter extends CursorAdapter {

    private final Context context;

    public FlashCardSetListAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        this.context = context;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.flashcard_sets_list_row, parent, false);
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        TextView word = (TextView) view.findViewById(R.id.flashcard_set_list_row_text);
        word.setText(cursor.getString(0));

        final PopupMenu actionsPopup = new PopupMenu(context, view);
        actionsPopup.getMenuInflater().inflate(R.menu.flashcard_set_list_row_popup_menu, actionsPopup.getMenu());
        final Long flashcardSetId = cursor.getLong(1);
        actionsPopup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.flashcard_set_list_popup_as_list) {
                    Intent intent = new Intent(context, BrowseFlashcardsListActivity.class);
                    intent.putExtra(BrowseFlashcardsListActivity.FLASHCARD_SET_ID, flashcardSetId);
                    context.startActivity(intent);
                } else if (item.getItemId() == R.id.flashcard_set_list_popup_reversed) {
                    Intent intent = new Intent(context, BrowseFlashcardsActivity.class);
                    intent.putExtra(BrowseFlashcardsActivity.FLASHCARD_SET_ID, flashcardSetId);
                    intent.putExtra(BrowseFlashcardsActivity.REVERSED, true);
                    context.startActivity(intent);
                }
                return false;
            }
        });

        final ImageButton actionButton = (ImageButton)view.findViewById(R.id.flashcard_set_list_row_actions);
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionsPopup.show();
            }
        });
    }
}
