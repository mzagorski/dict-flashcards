package com.mzagorski.dictFlashcards.adapters;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mzagorski.dictFlashcards.EditTranslationActivity;
import com.mzagorski.dictFlashcards.R;
import com.mzagorski.dictFlashcards.dao.DictFlashcardsDbHelper;
import com.mzagorski.dictFlashcards.dao.TranslationDao;
import com.mzagorski.dictFlashcards.model.Translation;
import com.mzagorski.dictFlashcards.service.FlashcardService;

public class DictListAdapter extends CursorAdapter {

    public static final int RESULT_LIST_MODIFIED = 1;


    private final FlashcardService flashCardService;
    private final TranslationDao translationDao;
    private String filterText;
    private final Activity activity;


    public DictListAdapter(Activity activity, Cursor cursor, int flags) {
        super(activity, cursor, flags);
        this.activity = activity;
        final DictFlashcardsDbHelper helper  = new DictFlashcardsDbHelper(activity);
        final SQLiteDatabase db = helper.getWritableDatabase();
        flashCardService = new FlashcardService(db);
        translationDao = new TranslationDao(db);
    }


    public int reloadCursor(final String filterText) {
        this.filterText = filterText;
        this.changeCursor(translationDao.findWordsInSelectedLang(filterText));
        return translationDao.countWordsInSelectedLang(filterText);
    }




    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.dict_list_row, parent, false);
    }

    @Override
    public void bindView(View view, final Context context, final Cursor cursor) {
        TextView word = (TextView) view.findViewById(R.id.dict_list_row_word);
        word.setText(cursor.getString(0));
        TextView transl = (TextView) view.findViewById(R.id.dict_list_row_translation);
        transl.setText(cursor.getString(1));

        final ImageButton addButton = (ImageButton)view.findViewById(R.id.dict_list_row_add_to_default_flashcard_set);
        final ImageButton deleteButton = (ImageButton)view.findViewById(R.id.dict_list_row_delete_from_default_flashcard_set);
        final long translationId = cursor.getLong(cursor.getColumnIndex("_id"));
        if(cursor.getString(2) != null) {
            addButton.setVisibility(View.GONE);
            deleteButton.setVisibility(View.VISIBLE);
            deleteButton.setEnabled(true);
            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteButton.setEnabled(false);
                    flashCardService.deleteFlashcardFromDefaultSet(translationId);
                    reloadCursor(filterText);
                }
            });
        } else {
            deleteButton.setVisibility(View.GONE);
            addButton.setVisibility(View.VISIBLE);
            addButton.setEnabled(true);
            addButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addButton.setEnabled(false);
                    flashCardService.addFlashCardToDefaultSet(translationId);
                    reloadCursor(filterText);
                }
            });
        }

        final PopupMenu actionsPopup = new PopupMenu(context, view);
        actionsPopup.getMenuInflater().inflate(R.menu.dict_list_row_popup_menu, actionsPopup.getMenu());
        actionsPopup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.dit_list_popup_delete) {
                    translationDao.delete(translationId);
                    reloadCursor(filterText);
                } else if (item.getItemId() == R.id.dit_list_popup_edit) {
                    Intent intent = new Intent(activity, EditTranslationActivity.class);
                    Translation t = new Translation();
                    t.id = translationId;
                    intent.putExtra(EditTranslationActivity.TRANSLATION, t);
                    activity.startActivityForResult(intent, RESULT_LIST_MODIFIED);
                }
                return false;
            }
        });

        final ImageButton actionButton = (ImageButton)view.findViewById(R.id.dict_list_row_action_button);
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionsPopup.show();
            }
        });
    }

}
