package com.mzagorski.dictFlashcards.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mzagorski.dictFlashcards.EditTranslationActivity;
import com.mzagorski.dictFlashcards.R;
import com.mzagorski.dictFlashcards.model.Translation;


public class OnlineDictListAdapter extends CursorAdapter {

    private final Activity activity;


    public OnlineDictListAdapter(Activity activity, Cursor cursor, int flags) {
        super(activity, cursor, flags);
        this.activity = activity;
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.online_dict_list_row, parent, false);
    }

    @Override
    public void bindView(View view, Context context, final Cursor cursor) {
        TextView wordView = (TextView) view.findViewById(R.id.online_dict_list_row_word);
        final String word = cursor.getString(0);
        wordView.setText(word);
        TextView translView = (TextView) view.findViewById(R.id.online_dict_list_row_translation);
        final String translation = cursor.getString(1);
        translView.setText(translation);

        ImageButton downloadButton = (ImageButton)view.findViewById(R.id.online_dict_list_row_download);
        downloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, EditTranslationActivity.class);
                Translation t = new Translation();
                t.word = word;
                t.translation = translation;
                intent.putExtra(EditTranslationActivity.TRANSLATION, t);
                activity.startActivityForResult(intent, DictListAdapter.RESULT_LIST_MODIFIED);
            }
        });
    }

}
