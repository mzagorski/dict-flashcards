package com.mzagorski.dictFlashcards.model;


public class Setting {
    public static final String KEY_TRANS_LANG_FROM = "TRANS_LANG_FROM";
    public static final String KEY_TRANS_LANG_TO = "TRANS_LANG_TO";
}
