package com.mzagorski.dictFlashcards.model;


public class Language {
    public Long id;
    public String englishName;
    public String code;
    public String iso639_3Code;


    public Language(){}

    public Language(Long id, String code, String englishName, String iso639_3Code){
        this.id = id;
        this.code = code;
        this.englishName = englishName;
        this.iso639_3Code = iso639_3Code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Language)) return false;

        Language language = (Language) o;

        if (id != null ? !id.equals(language.id) : language.id != null) return false;
        if (englishName != null ? !englishName.equals(language.englishName) : language.englishName != null) return false;
        if (code != null ? !code.equals(language.code) : language.code != null) return false;
        return !(iso639_3Code != null ? !iso639_3Code.equals(language.iso639_3Code) : language.iso639_3Code != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (englishName != null ? englishName.hashCode() : 0);
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (iso639_3Code != null ? iso639_3Code.hashCode() : 0);
        return result;
    }
}
