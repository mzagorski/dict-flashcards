package com.mzagorski.dictFlashcards.model;


public class FlashcardSet {
    public long id;
    public String name;
    public long langFrom;
    public long langTo;
    public boolean isDefault;
}
