package com.mzagorski.dictFlashcards.model;


import java.io.Serializable;

public class Translation implements Serializable {
    public long id;
    public String word;
    public String translation;
    public long langFrom;
    public long langTo;
}
