create table flashcard_set (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name varchar(100) UNIQUE NOT NULL,
  lang_from INTEGER NOT NULL,
  lang_to INTEGER NOT NULL,
  is_default SMALLINT NOT NULL DEFAULT 0,
  FOREIGN KEY(lang_from) REFERENCES language(id),
  FOREIGN KEY(lang_to) REFERENCES language(id)
);
