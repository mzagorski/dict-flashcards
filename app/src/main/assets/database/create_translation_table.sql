create table translation (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  lang_from INTEGER NOT NULL,
  lang_to INTEGER NOT NULL,
  word varchar(500) NOT NULL,
  translation varchar(500) NOT NULL,
  FOREIGN KEY(lang_from) REFERENCES language(id),
  FOREIGN KEY(lang_to) REFERENCES language(id)
);
