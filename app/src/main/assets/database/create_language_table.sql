create table language (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  code varchar(2) UNIQUE NOT NULL,
  english_name varchar(100) NOT NULL
);
