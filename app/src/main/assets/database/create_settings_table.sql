create table settings (
   id INTEGER PRIMARY KEY AUTOINCREMENT,
   key varchar(100) UNIQUE NOT NULL,
   value varchar(100) NOT NULL
);