create table flashcard (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  translation_id INTEGER NOT NULL,
  set_id INTEGER NOT NULL,
  FOREIGN KEY(translation_id) REFERENCES translation(id),
  FOREIGN KEY(set_id) REFERENCES flashcard_set(id),
  UNIQUE(translation_id, set_id)
);
